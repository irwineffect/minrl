use minrl::*;

use std::io::Read;
use std::io::Write;
use termios::{Termios, TCSANOW, ECHO, ICANON, tcsetattr};

struct CoreStdout<'a>(&'a mut std::io::Stdout);

impl<'a> core::fmt::Write for CoreStdout<'a> {
    fn write_str(&mut self, s: &str) -> Result<(), core::fmt::Error> {
        self.0.write(s.as_bytes()).unwrap();
        Ok(())
    }
}

fn main() {
    // This code turns off line buffering and echoing, since this library provides these features
    let orig_termios = Termios::from_fd(0).unwrap();
    let mut new_termios = orig_termios.clone();

    new_termios.c_lflag &= !(ICANON | ECHO);

    tcsetattr(0, TCSANOW, &mut new_termios).unwrap();

    let mut stdin = std::io::stdin();
    let mut b = [0u8; 1];

    // Allocate a buffer larger than the longest expected line
    let mut buffer = [0u8; 128];
    // Initialize a set of strings for completion
    let completions = ["app", "apple", "application", "banana", "hello", "world"];
    // Create the read line parser, providing it the buffer and list of completions
    let mut c = Minrl::new(&mut buffer, &completions);

    let mut stdout = std::io::stdout();
    let mut stdout = CoreStdout(&mut stdout);
    c.redraw_prompt(&mut stdout).unwrap();
    stdout.0.flush().unwrap();
    loop {
        // Read one byte
        stdin.read_exact(&mut b).unwrap();
        let b = b[0];
        // Try to feed the byte to the parser, also provide a handle to the output object
        match c.add_byte(b, &mut stdout) {
            // Returns Ok if a lines was entered, terms is an iterator over the terms
            Ok(mut terms) => {
                // Check the first term for "quit"
                match terms.next() {
                    Some("quit") => {
                        println!("goodbye!");
                        break;
                    }
                    Some(t) => {
                        println!("{}", t);
                    }
                    None => {}
                }

                // just print all remaining terms
                for t in terms {
                    println!("{}", t);
                }
                c.redraw_prompt(&mut stdout).unwrap();
            }
            // Don't worry if we need more bytes or the buffer is full
            Err(MinrlError::NeedMore) |
                Err(MinrlError::BufferFull) => {},
            // Any other errors, panic
            Err(e) => {panic!(e)}

        }
        stdout.0.flush().unwrap();
    }

    // Restore original terminal settings
    tcsetattr(0, TCSANOW, &orig_termios).unwrap();
}
