#![no_std]
#![no_main]

// pick a panicking behavior
//use panic_halt as _; // you can put a breakpoint on `rust_begin_unwind` to catch panics
// use panic_abort as _; // requires nightly
// use panic_itm as _; // logs messages over ITM; requires ITM support
use panic_semihosting as _; // logs messages to the host stderr; requires a debugger

use cortex_m_rt::entry;
use  stm32f1xx_hal as hal;
use hal::{pac, prelude::*};
use nb::block;

use core::fmt::Write;

use minrl::*;


#[entry]
fn main() -> ! {
let _cp = cortex_m::Peripherals::take().unwrap();
    let dp = pac::Peripherals::take().unwrap();

    let mut flash = dp.FLASH.constrain();

    let mut rcc =dp.RCC.constrain();
    let clocks = rcc.cfgr.freeze(&mut flash.acr);
    let mut afio = dp.AFIO.constrain(&mut rcc.apb2);
    let mut gpioa = dp.GPIOA.split(&mut rcc.apb2);


    let pin_tx = gpioa.pa9.into_alternate_push_pull(&mut gpioa.crh);
    let pin_rx = gpioa.pa10;

    let serial = hal::serial::Serial::usart1 (
        dp.USART1,
        (pin_tx, pin_rx),
        &mut afio.mapr,
        hal::serial::Config::default().baudrate(115200.bps()),
        clocks,
        &mut rcc.apb2
        );

    let (mut tx, mut rx) = serial.split();

    writeln!(tx, "init_complete").unwrap();

    // Allocate a buffer larger than the longest expected line
    let mut buffer = [0u8; 128];
    // Initialize a set of strings for completion
    let completions = ["app", "apple", "application", "banana", "hello", "world"];
    // Create the read line parser
    let mut c = Minrl::new(&mut buffer, &completions);

    c.redraw_prompt(&mut tx).unwrap();
    loop {
        // Read one byte
        let b = block!(rx.read()).unwrap();

        // Try to feed the byte to the parser, also provide a handle to the output object
        match c.add_byte(b, &mut tx) {
            // Returns Ok if a lines was entered, terms is an iterator over the terms
            Ok(mut terms) => {
                // Process first term
                let first_term = terms.next();
                match first_term {
                    Some("app") => {
                        writeln!(tx, "running app!").unwrap();
                    }
                    Some("apple") => {
                        writeln!(tx, "running apple, remaining terms:").unwrap();
                        for t in terms {
                            write!(tx, "{}\n", t).unwrap();
                        }
                    }
                    // Generic term handling
                    Some(t) => {
                        writeln!(tx, "running generic for \"{}\", remaining terms:", t).unwrap();
                        for t in terms {
                            write!(tx, "{}\n", t).unwrap();
                        }
                    }
                    None => {
                        // Ignore if nothing was entered
                    }
                }
                // Make sure to redraw the prompt after you have printed out your response
                c.redraw_prompt(&mut tx).unwrap();
            }
            // I don't care if we need more bytes or the buffer is full
            Err(MinrlError::NeedMore) |
                Err(MinrlError::BufferFull) => {},
            // Any other errors, panic
            Err(e) => {panic!("{:#?}", e)}
        }
    }
}
