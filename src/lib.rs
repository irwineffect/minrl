#![no_std]
#![deny(warnings)]

use core::fmt::Write;

type Terms<'a> = core::str::SplitAsciiWhitespace<'a>;

#[derive(Debug)]
pub enum MinrlError {
    NeedMore,
    BufferFull,
    FmtError(core::fmt::Error),
    Utf8Error(core::str::Utf8Error),
}

impl From<core::fmt::Error> for MinrlError  {
    fn from(err: core::fmt::Error) -> Self {
        MinrlError::FmtError(err)
    }
}

impl From<core::str::Utf8Error> for MinrlError  {
    fn from(err: core::str::Utf8Error) -> Self {
        MinrlError::Utf8Error(err)
    }
}

pub struct Minrl<'a> {
    buffer: &'a mut [u8],
    used: usize,
    completions: &'a [&'a str],
}

impl<'a> Minrl<'a> {
    pub fn new(buffer: &'a mut [u8], completions: &'a [&'a str]) -> Self {
        Minrl {
            buffer: buffer,
            used: 0,
            completions: completions,
        }
    }

    pub fn redraw_prompt<Out: Write>(&self, out: &mut Out) -> Result<(), MinrlError> {
        let s = core::str::from_utf8(&self.buffer[..self.used])?;
        write!(out, "\r> {}", s)?;

        Ok(())
    }

    fn autocomplete<Out: Write>(&mut self, out: &mut Out) -> Result<(), MinrlError> {
        if self.used > 0 {
            // Get the current term
            let t = if self.buffer[self.used - 1] == b' ' {
                ""
            } else {
                let b = &self.buffer[..self.used];
                core::str::from_utf8(b)
                    .unwrap_or("")
                    .split_ascii_whitespace()
                    .last().unwrap_or("")

            };

            let mut match_count = 0usize;
            for c in self.completions {
                if Self::partial_match(t, c) {
                    match_count += 1;
                }
            }

            match match_count {
                0 => {}
                1 => {
                    for c in self.completions {
                        if Self::partial_match(t, c) {
                            let remaining = c.len() - t.len();
                            let cslice = &c.as_bytes()[c.len() - remaining..c.len()];
                            let bslice = &mut self.buffer[self.used..self.used + remaining];
                            bslice.copy_from_slice(cslice);
                            self.used += remaining;

                            // Add a space after autocompleting
                            self.buffer[self.used] = b' ';
                            self.used += 1;
                            break;
                        }
                    }
                }
                _ => {
                    write!(out, "\n")?;
                    for c in self.completions {
                        if Self::partial_match(t, c) {
                            write!(out, "{} ", c)?;
                        }
                    }
                    write!(out, "\n")?;
                }
            }
        } else {
            // Nothing has been entered on the prompt yet, so print all completions
            write!(out, "\n")?;
            for c in self.completions {
                write!(out, "{} ", c)?;
            }
            write!(out, "\n")?;
        }

        Ok(())
    }

    fn partial_match(sample: &str, reference: &str) -> bool {
        let mut i = 0;

        // Compare characters between the sample and reference strings
        while i < sample.len() && i < reference.len() {
            // If there is a mismatch, break the loop
            if sample.as_bytes()[i] != reference.as_bytes()[i] {
                break;
            }
            i += 1;
        }

        // If we matched every character in the sample string, then we have a
        // partial match!
        if i == sample.len() {
            true
        } else {
            false
        }
    }

    pub fn clear(&mut self) {
        self.used = 0;
    }

    pub fn add_byte<Out: Write>(&mut self, b: u8, out: &mut Out) ->
        Result<Terms, MinrlError> {
        //dbg!("got here");

        match b {
            // Backspace character
            b'\x7f' => {
                if self.used > 0 {
                    self.used -= 1;
                    write!(out, "\x08 \x08")?;
                }
                Err(MinrlError::NeedMore)
            }
            // Newline (enter key was pressed)
            b'\n' => {
                let terms = self.parse();
                write!(out, "\n")?;
                Ok(terms)
            }
            // Tab character, try to autocomplete
            b'\t' => {
                self.autocomplete(out)?;
                self.redraw_prompt(out)?;
                Err(MinrlError::NeedMore)
            }
            // Any other character, just add to the buffer
            _ => {
                if self.used < self.buffer.len() - 1 {
                    self.buffer[self.used] = b;
                    self.used += 1;
                    //dbg!(b);
                    let b = b as char;
                    write!(out, "{}", b)?;
                    Err(MinrlError::NeedMore)
                } else {
                    //dbg!("buffer full");
                    Err(MinrlError::BufferFull)
                }
            }
        }
    }

    pub fn parse(&mut self) -> Terms {
        let used = self.used;
        self.used = 0;
        let s = core::str::from_utf8(&self.buffer[..used]).unwrap_or("");
        s.split_ascii_whitespace()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new() {
        let mut buf = [0u8; 128];

        let cl = Minrl::new(&mut buf, &[]);
    }

    #[test]
    fn test_partial_match() {
        assert_eq!(Minrl::partial_match("app", "apple"), true);
        assert_eq!(Minrl::partial_match("app", "app"), true);
        assert_eq!(Minrl::partial_match("apple", "app"), false);
        assert_eq!(Minrl::partial_match("", "app"), true);
        assert_eq!(Minrl::partial_match("ba", "apple"), false);
        assert_eq!(Minrl::partial_match("ba", "banana"), true);
    }
}
